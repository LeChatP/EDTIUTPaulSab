package me.lechatp.edtiutpaulsab.edt;


import android.net.NetworkInfo;
import androidx.fragment.app.FragmentManager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import me.lechatp.edtiutpaulsab.EDTType;
import me.lechatp.edtiutpaulsab.networking.DownloadCallback;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * this class defines an departement and is implmenting CelcatNetworkType
 * it transform DownloadCallback<String> to DownloadCallback<CelcatNetworkType>
 */
public class Departement extends CelcatNetwork implements CelcatNetworkType {

    private static final String XML = "<?xml";
    private static final String RESOURCE = "resource";
    private static final String SSRESOURCE = "//"+ RESOURCE;
    private static final String TYPE = "type";
    private static final String NAME = "name";
    private static final String LINK = "link";
    private static final String HTML_XML = "\\.(html|xml|pdf)$";
    private static final String ICS = ".ics";
    private static final String URLFORMAT = "%1$s://%2$s/%3$s/%4$s";
    private Set<EDT> edt;
    private DownloadCallback<CelcatNetworkType> activityCallback;

    public Departement(String name, String url) throws MalformedURLException {
        super(name, new URL(url));
        this.edt = new HashSet<>();
    }

    public Departement(DepartementEnum de) {
        super(de.getName(), de.getUrl());
        this.edt = new HashSet<>();
    }

    public void setActivityCallback(DownloadCallback<CelcatNetworkType> activityCallback) {
        this.activityCallback = activityCallback;
    }

    /**
     * start downloading of DepartementList
     *
     * @param fm is FragmentManager
     */
    public void fillDepartement(FragmentManager fm) {
        if (fm == null) { //if function called not by an activity just fill with URL.inputStream()
            this.fillDepartement();
        } else { //else use NetworkFragment
            this.initFragment(fm);
            this.startDownload();
        }
    }

    private void fillDepartement() {
        String result = null;
        try {
            result = Utils.readURL(this.getLink());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (result != null) {
            result = result.substring(result.indexOf(XML));

            Document source = null;
            try {
                source = Utils.getDocument(result);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
            this.setDepartement(source);
        }
    }

    private void setDepartement(Document source) {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();
        NodeList res = null;
        try {
            res = (NodeList) xPath.compile(SSRESOURCE).evaluate(source, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        if (res != null) {
                for (int i = 1; i < res.getLength(); i++) {
                Node item = res.item(i);
                String type = item.getAttributes().getNamedItem(TYPE).getTextContent();
                //support only group and staff for the moment
                //TODO: Support for any type
                if (type.equalsIgnoreCase(EDTType.GROUP.name()) || type.equalsIgnoreCase(EDTType.STAFF.name())) {
                    try {
                        this.edt.add(this.getEDT(item));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private EDT getEDT(Node item) throws MalformedURLException {
        String type = item.getAttributes().getNamedItem(TYPE).getTextContent();
        String name = "", url = "";
        NodeList content = item.getChildNodes();
        for (int j = 0; j < content.getLength(); j++) {
            Node child = content.item(j);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) item;
                name = element.getElementsByTagName(NAME).item(0).getTextContent();
                url = element.getAttribute(LINK).replaceAll(HTML_XML, ICS);
            }
        }
        return new EDT(
                EDTType.valueOf(type.toUpperCase()),
                name,
                new URL(String.format(
                        URLFORMAT,
                        this.getLink().getProtocol(),
                        this.getLink().getHost(),
                        this.getName(),
                        url)));
    }

    public Set<EDT> getEdt() {
        return edt;
    }

    public void setEdt(Set<EDT> edt) {
        this.edt = edt;
    }

    @Override
    public void updateFromDownload(String result) {
        if (result != null) {
            Document source;
            try {
                source = Utils.getDocument(result);
            } catch (ParserConfigurationException e) {
                this.onProgressUpdate(Progress.ERROR,99);
                return;
            } catch (IOException e) {
                this.onProgressUpdate(Progress.ERROR,99);
                return;
            } catch (SAXException e) {
                this.onProgressUpdate(Progress.ERROR,99);
                return;
            } catch (StringIndexOutOfBoundsException e){
                this.onProgressUpdate(Progress.ERROR,99);
                return;
            }
            this.setDepartement(source);
            this.activityCallback.updateFromDownload(this);
        } else {
            this.activityCallback.updateFromDownload(null);
        }

    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        return this.activityCallback.getActiveNetworkInfo();
    }

    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        this.activityCallback.onProgressUpdate(progressCode, percentComplete);
    }

    @Override
    public void finishDownloading() {
        this.activityCallback.finishDownloading();
        super.finishDownloading();
    }
}
