package me.lechatp.edtiutpaulsab;

import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;

import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.edt.EDT;

/**
 * A login screen that offers login via email/password.
 */
public class AddFriendActivity extends BaseActivity implements AddEDT {

    private AddEDTActivityManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.manager = new AddEDTActivityManager(this,getSupportFragmentManager(), this, true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_friend;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.manager.onItemSelected(parent,view,position,id);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        this.manager.onNothingSelected(parent);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        this.manager.onCheckedChanged(group,checkedId);
    }

    @Override
    public void onClick(View v) {
        this.manager.onClick(v);
    }

    @Override
    public void updateFromDownload(CelcatNetworkType result) {
        if(result instanceof EDT){
            Intent intent = new Intent();
            intent.putExtra(getString(R.string.message_passing_toast),getString(R.string.friend_added));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public NetworkInfo getActiveNetworkInfo(){
        return null;
    }

    public void onProgressUpdate(int progressCode, int percentComplete) {
    }

    public void finishDownloading() {

    }

}

