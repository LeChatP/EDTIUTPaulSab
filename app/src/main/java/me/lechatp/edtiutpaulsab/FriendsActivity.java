package me.lechatp.edtiutpaulsab;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.edt.Jour;
import me.lechatp.edtiutpaulsab.utils.SqLite;

/**
 * Activité affichant les amis et si ils sont en cours
 */
public class FriendsActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    private static final int ADD_FRIEND = 0;
    private static final String EDT = "EDT";
    private static final String NAME = "NAME";
    private ListView listeAmis;
    private ArrayList<Friend> friendslist;
    private ArrayAdapter<Friend> friendsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent add = new Intent(FriendsActivity.this,AddFriendActivity.class);
            startActivityForResult(add,ADD_FRIEND);
        });
        listeAmis=findViewById(R.id.listAmis);
        listeAmis.setClickable(true);
        listeAmis.setLongClickable(true);
        listeAmis.setOnItemClickListener(this);
        listeAmis.setOnItemLongClickListener((parent, relativeLayout, position, id) -> {
            //Do your tasks here
            AlertDialog.Builder alert = new AlertDialog.Builder(FriendsActivity.this);
            alert.setTitle(getString(android.R.string.dialog_alert_title));
            alert.setMessage(getString(R.string.confirm));
            alert.setPositiveButton(getString(android.R.string.yes), (dialog, which) -> {
                TextView text = (TextView)((RelativeLayout)relativeLayout).getChildAt(0);
                SqLite.getInstance(FriendsActivity.this).removeFriend(
                        text.getText().toString().substring(
                                0,text.getText().toString().indexOf('\n')
                        )
                );
                friendslist.remove(position);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(FriendsActivity.this,getString(R.string.friend_removed),Toast.LENGTH_LONG).show();
                dialog.dismiss();
            });
            alert.setNegativeButton(getString(android.R.string.no), (dialog, which) -> dialog.dismiss());

            alert.show();

            return true;


        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_friends;
    }

    /**
     * Affiche les amis
     */
    @Override
    protected void onResume() {
        super.onResume();
        Cursor c=this.getDb().getAllAmis();

        this.friendslist=new ArrayList<>();
        while(c.moveToNext()){
            Friend f = new Friend(c.getString(SqLite.AMIS_COLONNES.NOM.getValue()),c.getString(SqLite.AMIS_COLONNES.NAMEEDT.getValue()));
            EDT edtami = this.getDb().getEDT(c.getString(SqLite.AMIS_COLONNES.NAMEEDT.getValue()));
            if(edtami != null) {
                Jour jour = edtami.getToday();
                if (jour != null){
                    Calendar cal = new GregorianCalendar();
                    f.setCreneau(jour.getLastCreneau(cal));
                }

            }

            this.friendslist.add(f);
        }
        c.close();
        friendsAdapter = new FriendsArrayAdapter(getBaseContext(), this.friendslist);
        listeAmis.setAdapter(friendsAdapter);
        if(getIntent().hasExtra(getString(R.string.message_passing_toast))) Toast.makeText(this,getIntent().getStringExtra(getString(R.string.message_passing_toast)),Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // On vérifie tout d'abord à quel intent on fait référence ici à l'aide de notre identifiant
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_FRIEND) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, data.getStringExtra(getString(R.string.message_passing_toast)), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void updateFromDownload(CelcatNetworkType result) {

    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        return null;
    }

    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {

    }

    @Override
    public void finishDownloading() {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Friend f = (Friend) parent.getAdapter().getItem(position);
        String edtname = f.getGroup();
        Intent i = new Intent(FriendsActivity.this,MainActivity.class);
        i.putExtra(EDT,edtname);
        i.putExtra(NAME,f.getFriend());
        startActivity(i);
    }
}
