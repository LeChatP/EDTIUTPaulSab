package me.lechatp.edtiutpaulsab.edt;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;

import androidx.annotation.NonNull;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * Organise les crénaux en jours pour simplifier la récupération des Créneaux
 */
public class Jour implements Parcelable {
    private static final String CET = "CET";
    private Calendar jour;
    private SortedSet<Creneau> creneaux;

    Jour(Calendar jour){
        this.jour = Utils.removeTimeOf(jour);
        this.creneaux = new TreeSet<>(new DateCreneauComparator());
    }

    boolean setCreneau(Creneau c){
        if(Utils.sameDay(this.jour,c.getDebut()))
        {
            this.creneaux.add(c);
            return true;
        }
        return false;
    }

    public Calendar getDate() {
        return jour;
    }

    public SortedSet<Creneau> getCreneaux(){
        return this.creneaux;
    }

    /**
     * Donne le créneau de la date donnée
     * @param c la date entre les deux créneaux
     * @return Creneau
     */
    public Creneau getLastCreneau(Calendar c){
        for(Creneau creneau : this.getCreneaux()){
            Calendar debut = creneau.getDebut();
            Calendar fin = creneau.getFin();
            if(fin.compareTo(c) > 0 && debut.compareTo(c) < 0) return creneau;
        }
        return null;
    }

    @Override
    @NonNull
    public String toString(){
        StringBuilder returned = new StringBuilder(Utils.dayToString(this.jour) + " : \n{");
        for(Creneau c : this.creneaux){
            returned.append("\n").append(c.toString());
        }
        returned.append("}\n");
        return returned.toString();
    }


    private Jour(Parcel in){
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(in.readLong());
        this.jour = c;
        this.creneaux = new TreeSet<>(new DateCreneauComparator());
        in.readTypedList(new ArrayList<>(this.creneaux),Creneau.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.jour.getTimeInMillis());
        dest.writeTypedList(new ArrayList<>(this.creneaux));
    }

    public static final Parcelable.Creator<Jour> CREATOR = new Parcelable.Creator<Jour>() {
        @Override
        public Jour createFromParcel(Parcel source) {
            return new Jour(source);
        }

        @Override
        public Jour[] newArray(int size) {
            return new Jour[size];
        }
    };
}
