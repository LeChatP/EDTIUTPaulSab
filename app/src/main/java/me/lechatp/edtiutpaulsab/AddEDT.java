package me.lechatp.edtiutpaulsab;

import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;

import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.networking.DownloadCallback;

interface AddEDT extends RadioGroup.OnCheckedChangeListener, Button.OnClickListener, Spinner.OnItemSelectedListener , DownloadCallback<CelcatNetworkType> {
}
