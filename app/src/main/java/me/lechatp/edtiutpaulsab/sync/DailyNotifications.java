package me.lechatp.edtiutpaulsab.sync;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Calendar;

import androidx.core.app.NotificationCompat;
import me.lechatp.edtiutpaulsab.MainActivity;
import me.lechatp.edtiutpaulsab.R;
import me.lechatp.edtiutpaulsab.edt.Creneau;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.edt.Jour;
import me.lechatp.edtiutpaulsab.utils.SqLite;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * This class set all notifications for the day
 */
public class DailyNotifications extends BroadcastReceiver {

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int CINQMIN = MINUTE*5;
    private static final int ZERO = 0;
    private static final int DAY = 86400000;
    private static final String TU_AS = "Tu as ";
    private static final String SALLE = " salle ";
    private static final String MATIERE = " pour la matière ";
    private static final String DANS_5_MINUTES = " dans 5 minutes";

    /**
     * When new day or enable notification system, schedule all Creneau notifications.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        EDT edt = SqLite.getInstance(context).getMyEDT();
        if(edt != null && edt.getToday() != null)
            actionPendingIntents(context,edt.getToday(),true);
    }

    public static void initNotifications(Context context,boolean schedule){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(context.getString(R.string.channel_id), context.getString(R.string.notify_channel_desc),NotificationManager.IMPORTANCE_DEFAULT);
            channel.enableVibration(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION),
                    new AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build());
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
            schedule = schedule && channel.getImportance() != NotificationManager.IMPORTANCE_NONE;
        }else{
            schedule = schedule && prefs.getBoolean(context.getString(R.string.enable_notifications),true);
        }
        DailyNotifications.scheduleDailyNotifications(context,schedule);
        EDT myedt = SqLite.getInstance(context).getMyEDT();
        if(myedt != null && myedt.getToday() != null)
            DailyNotifications.actionPendingIntents(context, myedt.getToday(),schedule);
    }

    public static void scheduleDailyNotifications(Context context,boolean schedule) {
        int id = Calendar.getInstance().get(Calendar.DATE);
        Intent notificationIntent = new Intent(context, DailyNotifications.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar c = Calendar.getInstance();
        c = Utils.removeTimeOf(c);
        c.add(Calendar.DATE, 1);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(alarmManager != null)
        if(schedule) {
            alarmManager.setRepeating(AlarmManager.RTC, c.getTimeInMillis(), DAY, pendingIntent);
        }else{
            alarmManager.cancel(pendingIntent);
        }
    }

    public static void actionPendingIntents(Context context,Jour j,boolean schedule){
        if (j != null)
            for (Creneau c : j.getCreneaux()) {
                if (c.getDebut().compareTo(Calendar.getInstance()) > ZERO) {
                    if (schedule) {
                        scheduleNotification(context.getApplicationContext(),
                                             getNotification(context.getApplicationContext(),
                                                     c.getType(),
                                                     c.getSalle(),
                                                     c.getMatiere(),
                                                     c.getDebut().getTimeInMillis()-CINQMIN,
                                                     c.getId()),
                                        c.getDebut().getTimeInMillis() - CINQMIN, (int)c.getId());
                    } else {
                        cancelNotification(context.getApplicationContext(), (int)c.getId());
                    }
                }
            }
    }
    private static void scheduleNotification(Context context,Notification notification,long time,long id) {
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION,notification);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID,id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int)id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.setExact(AlarmManager.RTC, time,pendingIntent);
        }
    }

    private static void cancelNotification(Context context,long id) {
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int)id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    private static Notification getNotification(Context context, String type, String salle, String matiere, long when, long code){
        if(type.isEmpty()) type = "cours";
        if(salle.isEmpty()) salle = "inconnue";
        if(matiere.isEmpty()) matiere = "inconnue";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new Notification.Builder(context,context.getString(R.string.channel_id))
                    .setContentTitle(context.getString(R.string.app_name)) // title for notification
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(TU_AS +type+ SALLE +salle+ MATIERE +matiere+ DANS_5_MINUTES))
                    // message for notification
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setColor(context.getColor(R.color.colorPrimary))
                    .setColorized(true)
                    .setWhen(when)
                    .setShowWhen(true)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            R.mipmap.ic_launcher))
                    .setContentIntent(PendingIntent.getActivity(context, (int)code, new Intent(context, MainActivity.class), 0))
                    .setAutoCancel(true)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .build();
        }else{
            return new NotificationCompat.Builder(context,context.getString(R.string.channel_id))
                    .setContentTitle(context.getString(R.string.app_name)) // title for notification
                    .setContentText(TU_AS +type+ SALLE +salle+ MATIERE +matiere+DANS_5_MINUTES)// message for notification
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(TU_AS +type+ SALLE +salle+ MATIERE +matiere+ DANS_5_MINUTES))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            R.mipmap.ic_launcher))
                    .setContentIntent(PendingIntent.getActivity(context, (int)code, new Intent(context, MainActivity.class), 0))
                    .setAutoCancel(true)
                    .setWhen(when)
                    .setShowWhen(true)
                    .setSound(Uri.parse(prefs.getString("ringtone_uri", RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).getPath())))
                    .setVibrate((prefs.getBoolean(context.getString(R.string.notifications_vibration),true) ? new long[] {SECOND, SECOND, SECOND, SECOND, SECOND} : new long[] {0}))
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .build();
        }
    }
}
