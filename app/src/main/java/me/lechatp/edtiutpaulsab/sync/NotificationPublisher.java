package me.lechatp.edtiutpaulsab.sync;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import me.lechatp.edtiutpaulsab.utils.SqLite;

public class NotificationPublisher extends BroadcastReceiver {
    private static final String EDT_IUT_PAUL_SAB = "EDT IUT Paul Sab";
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";

    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        SqLite sql = SqLite.getInstance(context);
        if(!sql.isCreneauNotified(id)) {
            if (notificationManager != null) {
                notificationManager.notify(EDT_IUT_PAUL_SAB, id, notification);
            }
            sql.beginTransaction();
            boolean notino = sql.setNotified(id);
            sql.commit();
        }
    }

}
