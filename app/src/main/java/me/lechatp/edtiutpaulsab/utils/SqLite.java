package me.lechatp.edtiutpaulsab.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import me.lechatp.edtiutpaulsab.EDTType;
import me.lechatp.edtiutpaulsab.edt.Creneau;
import me.lechatp.edtiutpaulsab.edt.EDT;

/**
 * Classe gstionnaire de BDD
 */
public class SqLite extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="edt.db";
    private static final String TABLE_NAME_EDT ="Edt";
    private static final String TABLE_NAME_AMIS = "Amis";
    private static final String TABLE_NAME_CRENEAU = "Creneau";
    private static final String TABLE_NAME_SYSTEM = "System";
    private static final String CREATE_TABLE = "CREATE TABLE";
    private static final String SELECT = "SELECT";
    private static final String FROM = "FROM";
    private static final String WHERE = "WHERE";
    private static final String EQ = "=";
    private static final String EQ_ARG = EQ+" ?";
    private static final String COMMA = ",";

    private static final String SPACE = " ";
    private static final String CREATE_TABLE_SYSTEM = CREATE_TABLE + SPACE + TABLE_NAME_SYSTEM+ "("+SYSTEM_COLONNES.LAST_SYNC+" DATETIME)";
    private static final String PRIMARY_KEY = "PRIMARY KEY";
    private static final String VARCHAR = "VARCHAR";
    private static final String CREATE_TABLE_EDT = CREATE_TABLE + SPACE + TABLE_NAME_EDT + "("
            + EDT_COLONNES.NAME + " " + VARCHAR + "(41) " + PRIMARY_KEY +COMMA
            + EDT_COLONNES.TYPE + " " + VARCHAR + "(10)" +COMMA
            + EDT_COLONNES.URL + " " + VARCHAR + "(255)" +COMMA
            + EDT_COLONNES.ISMINE + " LOGICAL)";
    private static final String INTEGER = "INTEGER";
    private static final String AUTOINCREMENT = "AUTOINCREMENT";
    private static final String CREATE_TABLE_AMIS = CREATE_TABLE + SPACE + TABLE_NAME_AMIS + "("
            + AMIS_COLONNES.ID + " " + INTEGER + " " + PRIMARY_KEY + " " + AUTOINCREMENT +COMMA
            + AMIS_COLONNES.NOM + " " + VARCHAR + "(20) UNIQUE" +COMMA
            + AMIS_COLONNES.NAMEEDT + " " + VARCHAR + "(41) NOT NULL" +COMMA
            + " FOREIGN KEY(" + AMIS_COLONNES.NAMEEDT + ") REFERENCES " + TABLE_NAME_EDT + "(" + EDT_COLONNES.NAME + "))";
    private static final String DATETIME = " DATETIME";
    private static final String CREATE_TABLE_CRENEAU = CREATE_TABLE + SPACE + TABLE_NAME_CRENEAU + "("
            + CRENEAU_COLONNES.ID + " " + INTEGER + " " + PRIMARY_KEY + " " + AUTOINCREMENT +COMMA
            + CRENEAU_COLONNES.DEBUT + DATETIME +COMMA
            + CRENEAU_COLONNES.FIN + DATETIME +COMMA
            + CRENEAU_COLONNES.TYPE + " " + VARCHAR + "(16)" +COMMA
            + CRENEAU_COLONNES.MATIERE + " " + VARCHAR + "(181)" +COMMA
            + CRENEAU_COLONNES.SALLE + " " + VARCHAR + "(64)" +COMMA
            + CRENEAU_COLONNES.ENSEIGNANT + " TEXT"+COMMA
            + CRENEAU_COLONNES.NAMEEDT + " " + VARCHAR + "(41) NOT NULL" +COMMA
            + CRENEAU_COLONNES.DETAILS.name()+" TEXT"+COMMA
            + CRENEAU_COLONNES.NOTIFIED.name()+ " " + INTEGER + " DEFAULT 0" +COMMA
            + " FOREIGN KEY(" + CRENEAU_COLONNES.NAMEEDT + ") REFERENCES " + TABLE_NAME_EDT + "(" + EDT_COLONNES.NAME + "))";
    private static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS ";
    private static final String GET_EDT = SELECT + SPACE +TABLE_NAME_EDT+".* " + FROM + SPACE + TABLE_NAME_EDT + SPACE + WHERE + SPACE + EDT_COLONNES.NAME + EQ_ARG;
    private static final String IS_AMI_EXIST = SELECT + SPACE + AMIS_COLONNES.NOM.name() + SPACE + FROM + SPACE + TABLE_NAME_AMIS + SPACE + WHERE + SPACE + AMIS_COLONNES.NOM.name() + EQ_ARG;
    private static final String IS_EDT_EXIST = SELECT + SPACE + EDT_COLONNES.NAME.name() + SPACE + FROM + SPACE + TABLE_NAME_EDT + SPACE + WHERE + SPACE + EDT_COLONNES.NAME.name() + EQ_ARG;

    private static final int DATABASE_VERSION = 14;
    private static final String COUNT_USED = "SELECT COUNT(*) " + FROM + SPACE + TABLE_NAME_AMIS + ", " + TABLE_NAME_EDT + SPACE + WHERE + SPACE + EDT_COLONNES.NAME.fullName() + SPACE+"="+SPACE + AMIS_COLONNES.NAMEEDT.fullName() + " AND " + EDT_COLONNES.NAME.fullName() + EQ_ARG;
    private static final String ADD_DETAILS_COL = String.format("ALTER TABLE %s ADD COLUMN %s text", TABLE_NAME_CRENEAU, CRENEAU_COLONNES.DETAILS.name());
    private static final String ADD_NOTIFIED_COL = String.format("ALTER TABLE %s ADD COLUMN %s " + INTEGER + " DEFAULT 0",TABLE_NAME_CRENEAU,CRENEAU_COLONNES.NOTIFIED);
    private static final String INSERT_INTO = "INSERT INTO ";
    private static final String VALUES = "VALUES";

    private static SqLite sInstance;

    public enum SYSTEM_COLONNES{
        LAST_SYNC(0);
        private int val;
        SYSTEM_COLONNES(int val){
            this.val = val;
        }
        public int getValue(){
            return val;
        }
    }
    public enum EDT_COLONNES{
        NAME(0),TYPE(1),URL(2),ISMINE(3);
        private int val;
        EDT_COLONNES(int v){
            this.val = v;
        }
        public int getValue(){
            return this.val;
        }
        @SuppressWarnings("unused")
        public String fullName(){ return TABLE_NAME_EDT +"."+this.name();}
    }
    public enum AMIS_COLONNES{
        ID(0),NOM(1),NAMEEDT(2);
        private int val;
        AMIS_COLONNES(int v){
            this.val = v;
        }
        public int getValue(){
            return this.val;
        }
        @SuppressWarnings("unused")
        public String fullName(){ return TABLE_NAME_AMIS +"."+this.name();}
    }
    public enum CRENEAU_COLONNES{
        ID(0),DEBUT(1),FIN(2),TYPE(3),MATIERE(4),SALLE(5),ENSEIGNANT(6),NAMEEDT(7),DETAILS(8),NOTIFIED(9);
        private int val;
        CRENEAU_COLONNES(int v){
            this.val = v;
        }
        public int getValue(){
            return this.val;
        }
        @SuppressWarnings("unused")
        public String fullName(){ return TABLE_NAME_CRENEAU +"."+this.name();}
    }

    /**
     * Access to singleton
     * @param context Context of application
     * @return SqLite current instance
     * Note : Generated Automatically
     */
    public static synchronized SqLite getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SqLite(context);
        }
        return sInstance;
    }


    private SqLite(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
       

    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_TABLE_EDT);
        db.execSQL(CREATE_TABLE_AMIS);
        db.execSQL(CREATE_TABLE_CRENEAU);
        db.execSQL(CREATE_TABLE_SYSTEM);
        String[] date = {"0000-00-00 00:00:00"};
        db.execSQL(INSERT_INTO +TABLE_NAME_SYSTEM+ SPACE + VALUES + " (?)",date);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        if(oldVersion <= 12){
            db.execSQL(ADD_DETAILS_COL);
        }
        if(oldVersion <= 13){
            db.execSQL(ADD_NOTIFIED_COL);
        }
    }
    @Override
    public void onDowngrade(SQLiteDatabase db,int oldVersion,int newVersion){
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_NAME_EDT);
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_NAME_AMIS);
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_NAME_CRENEAU);
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_NAME_SYSTEM);
        this.onCreate(db);
    }



    public void beginTransaction(){
        this.getWritableDatabase().beginTransaction();
    }

    public void commit(){
        this.getWritableDatabase().setTransactionSuccessful();
        this.getWritableDatabase().endTransaction();
    }

    public void rollback(){
        this.getWritableDatabase().endTransaction();
    }

    public boolean isFirstUse(){
        SQLiteDatabase db=this.getReadableDatabase();
        String[] cols = {EDT_COLONNES.ISMINE.name()};
        String[] args = {"1"};
        Cursor result=db.query(TABLE_NAME_EDT,cols,EDT_COLONNES.ISMINE.name()+EQ_ARG,args,null,null,null);
        int c = result.getCount();
        result.close();
        return c < 1;
    }

    public boolean isCreneauNotified(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] cols = { CRENEAU_COLONNES.NOTIFIED.name() };
        Cursor c = db.query(TABLE_NAME_CRENEAU,cols,CRENEAU_COLONNES.NOTIFIED.name()+EQ_ARG, new String[]{String.valueOf(id)},null,null,null);
        boolean result = false;
        c.moveToFirst();
        if (c.getCount() > 0)
            result = Integer.parseInt(c.getString(0)) == 1;
        c.close();
        return result;
    }

    public boolean setNotified(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(CRENEAU_COLONNES.NOTIFIED.name(),"1");
        return 1 != db.update(TABLE_NAME_CRENEAU,cv,CRENEAU_COLONNES.ID.name()+EQ_ARG, new String[]{String.valueOf(id)});
    }

    private boolean isEdtExist(String edtNom){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] values = {edtNom};
        Cursor c = db.rawQuery(IS_EDT_EXIST,values);
        boolean result =c.getCount() > 0;
        c.close();
        return result;
    }

    public boolean isAmisExist(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] values = {name};
        Cursor c = db.rawQuery(IS_AMI_EXIST,values);
        boolean result = c.getCount() > 0;
        c.close();
        return result;
    }

    public void insertDataAmis(String nom, String edtName){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues valeur=new ContentValues();
        valeur.put(AMIS_COLONNES.NOM.name(),nom);
        valeur.put(AMIS_COLONNES.NAMEEDT.name(),edtName);
        db.insert(TABLE_NAME_AMIS,null,valeur);
    }

    public Cursor getAllAmis(){
        SQLiteDatabase db=this.getReadableDatabase();
        String[] strings = {};
        return db.query(TABLE_NAME_AMIS,null,"",strings,null,null,null);
    }

    public boolean insertDataEdt(String nom, String type, String url, Boolean b){
        if(!isFirstUse() && b) {
            return false; //deny multiple
        }
        if(this.isEdtExist(nom)) {
            return false;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues valeur=new ContentValues();
        valeur.put(EDT_COLONNES.NAME.name(),nom);
        valeur.put(EDT_COLONNES.TYPE.name(),type);
        valeur.put(EDT_COLONNES.URL.name(),url);
        valeur.put(EDT_COLONNES.ISMINE.name(),b);
        long result=db.insert(TABLE_NAME_EDT,null,valeur);
        return result != -1;
    }

    public void setMine(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(EDT_COLONNES.ISMINE.name(),1);
        String[] args = {name};
        db.update(TABLE_NAME_EDT, cv, EDT_COLONNES.NAME.name() + EQ_ARG, args);
    }

    public boolean insertDataCreneaux(String nomEdt, Set<Creneau> creneaux){
        SQLiteDatabase db = this.getWritableDatabase();
        for(Creneau c : creneaux){
            ContentValues valeur = new ContentValues();
            valeur.put(CRENEAU_COLONNES.DEBUT.name(), Utils.toDatetime(c.getDebut()));
            valeur.put(CRENEAU_COLONNES.FIN.name(),Utils.toDatetime(c.getFin()));
            valeur.put(CRENEAU_COLONNES.TYPE.name(),c.getType());
            valeur.put(CRENEAU_COLONNES.MATIERE.name(),c.getMatiere());
            valeur.put(CRENEAU_COLONNES.SALLE.name(),c.getSalle());
            valeur.put(CRENEAU_COLONNES.ENSEIGNANT.name(),c.getEnseignant());
            valeur.put(CRENEAU_COLONNES.NAMEEDT.name(),nomEdt);
            valeur.put(CRENEAU_COLONNES.DETAILS.name(),c.getDetails());
            long result = db.insert(TABLE_NAME_CRENEAU,null,valeur);
            if(result ==-1){
                return false;
            }
            c.setId(result);
        }
        return true;
    }

    public void removeMineEDT(){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] arg = {"1"};
        ContentValues cv = new ContentValues();
        cv.put(EDT_COLONNES.ISMINE.name(),0);
        Cursor mine = this.getMine();
        if (mine != null) {
            mine.moveToFirst();
            String name = mine.getString(EDT_COLONNES.NAME.getValue());
            if (this.countUsed(mine.getString(EDT_COLONNES.NAME.getValue())) > 0) {
                db.update(TABLE_NAME_EDT, cv, EDT_COLONNES.ISMINE + EQ_ARG, arg);

            } else {
                arg[0] = name;
                db.delete(TABLE_NAME_EDT, EDT_COLONNES.NAME + EQ_ARG, arg);
                db.delete(TABLE_NAME_CRENEAU, CRENEAU_COLONNES.NAMEEDT + EQ_ARG, arg);
            }
        }
    }

    private int countUsed(String edtName){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] args = {edtName};
        Cursor c= db.rawQuery(COUNT_USED, args);
        c.moveToFirst();
        int res = c.getInt(0);
        c.close();
        return res;
    }

    private boolean isMine(String edtName){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] args = {"1",edtName};
        String[] cols = {EDT_COLONNES.ISMINE.name()};
        Cursor c = db.query(TABLE_NAME_EDT,cols,EDT_COLONNES.ISMINE.name()+EQ_ARG+" AND "+EDT_COLONNES.NAME+EQ_ARG,args,null,null,null);
        c.moveToFirst();
        boolean res = c.getCount()>0;
        c.close();
        return res;
    }

    public void updateSync(){
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SYSTEM_COLONNES.LAST_SYNC.name(),Utils.toDatetime(new GregorianCalendar()));
        db.update(TABLE_NAME_SYSTEM,cv,null,null);
    }

    public Calendar getLastSync(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_SYSTEM,null,null,null,null,null,null);
        c.moveToFirst();
        Calendar r = Utils.getDatetime(c.getString(SYSTEM_COLONNES.LAST_SYNC.getValue()));
        c.close();
        return r;
    }

    public Set<EDT> getAllEDT(){
        Set<EDT> edts = new HashSet<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME_EDT,null,null,null,null,null,null);
        while(c.moveToNext()){
            try {
                edts.add(new EDT(EDTType.valueOf(c.getString(EDT_COLONNES.TYPE.getValue())),c.getString(EDT_COLONNES.NAME.getValue()),new URL(c.getString(EDT_COLONNES.URL.getValue()))));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        c.close();
        return edts;
    }

    public void emptyCreneaux(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_NAME_CRENEAU);
        db.execSQL(CREATE_TABLE_CRENEAU);
    }

    private Cursor getAllCreneau(EDT edt){
        SQLiteDatabase db=this.getReadableDatabase();
        String[] args = {edt.getName()};
        return db.query(TABLE_NAME_CRENEAU,null,CRENEAU_COLONNES.NAMEEDT + EQ_ARG,args,null,null,null);
    }

    private Cursor getFriend(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] args = {name};
        return db.query(TABLE_NAME_AMIS,null,AMIS_COLONNES.NOM+EQ_ARG,args,null,null,null);
    }

    public void removeFriend(String name){
        Cursor friend = this.getFriend(name);
        friend.moveToFirst();
        String edtName = friend.getString(AMIS_COLONNES.NAMEEDT.getValue());
        friend.close();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] args = {name};
        db.delete(TABLE_NAME_AMIS,AMIS_COLONNES.NOM+EQ_ARG,args);
        if(!this.isMine(edtName) && this.countUsed(edtName) <0){
            args[0] = edtName;
            db.delete(TABLE_NAME_EDT,EDT_COLONNES.NAME.name()+EQ_ARG,args);
            db.delete(TABLE_NAME_CRENEAU,CRENEAU_COLONNES.NAMEEDT+EQ_ARG,args);
        }
    }

    private Cursor getMine(){
        if(isFirstUse()) return null;
        SQLiteDatabase db=this.getReadableDatabase();
        String[] args = {"1"};
        return db.query(TABLE_NAME_EDT,null,EDT_COLONNES.ISMINE.name()+ EQ_ARG,args,null,null,null);
    }

    public EDT getMyEDT(){
        Cursor result = getMine();
        if (result != null) {
            result.moveToFirst();
            EDT edt = null;
            try {
                edt=new EDT(
                        EDTType.valueOf(result.getString(EDT_COLONNES.TYPE.getValue()).toUpperCase()),
                        result.getString(EDT_COLONNES.NAME.getValue()),
                        new URL(result.getString(EDT_COLONNES.URL.getValue())));
                Cursor c=this.getAllCreneau(edt);
                while(c.moveToNext()){
                    edt.addCreneau(new Creneau(
                            c.getInt(CRENEAU_COLONNES.ID.getValue()),
                            Utils.getDatetime(c.getString(CRENEAU_COLONNES.DEBUT.getValue())),
                            Utils.getDatetime(c.getString(CRENEAU_COLONNES.FIN.getValue())),
                            c.getString(CRENEAU_COLONNES.TYPE.getValue()),
                            c.getString(CRENEAU_COLONNES.SALLE.getValue()),
                            c.getString(CRENEAU_COLONNES.MATIERE.getValue()),
                            c.getString(CRENEAU_COLONNES.ENSEIGNANT.getValue()),
                            c.getString(CRENEAU_COLONNES.DETAILS.getValue())));
                }
                c.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            result.close();
            return edt;
        }
        return null;
    }

    public EDT getEDT(String nom) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] args = {nom};
        Cursor result = db.rawQuery(GET_EDT, args);
        if(result.moveToFirst()) {
            EDT edt = null;
            try {
                edt = new EDT(EDTType.valueOf(result.getString(EDT_COLONNES.TYPE.getValue()).toUpperCase()), result.getString(EDT_COLONNES.NAME.getValue()), new URL(result.getString(EDT_COLONNES.URL.getValue())));
                Cursor c = this.getAllCreneau(edt);
                while (c.moveToNext()) {
                    edt.addCreneau(new Creneau(
                            c.getInt(CRENEAU_COLONNES.ID.getValue()),
                            Utils.getDatetime(c.getString(CRENEAU_COLONNES.DEBUT.getValue())),
                            Utils.getDatetime(c.getString(CRENEAU_COLONNES.FIN.getValue())),
                            c.getString(CRENEAU_COLONNES.TYPE.getValue()),
                            c.getString(CRENEAU_COLONNES.SALLE.getValue()),
                            c.getString(CRENEAU_COLONNES.MATIERE.getValue()),
                            c.getString(CRENEAU_COLONNES.ENSEIGNANT.getValue())));
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            result.close();
            return edt;
        }
        return null;
    }


}
