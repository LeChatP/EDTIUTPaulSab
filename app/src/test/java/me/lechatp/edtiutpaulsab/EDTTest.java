package me.lechatp.edtiutpaulsab;

import junit.framework.TestCase;

import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;


import me.lechatp.edtiutpaulsab.edt.EDT;

public class EDTTest extends TestCase {
    private EDT d;
    @Override
    public void setUp() throws MalformedURLException {
        this.d = new EDT(EDTType.GROUP,"S1A",new URL("https://edt.iut-tlse3.fr/planning/info/g8647.ics"));
    }
    @Override
    public void tearDown(){
        this.d = null;
    }
    @Test(expected=MalformedURLException.class)
    public void testExcpetionEDT() throws MalformedURLException {
        // this.d = new EDT("test","\\/;)WrongURL");
    }
    @Test
    public void testGetName(){
        assertEquals(this.d.getName(),"S1A");
    }
    @Test
    public void testFillEDT() throws IOException, ParseException {
        this.d.fillEDT(null);
        assertFalse(this.d.getJours().isEmpty());// maybe an false-positive if calendar is really empty in celcat
    }
    @Test
    public void testGetNextDay() throws IOException, ParseException {
        this.d.fillEDT(null);
        assertNotNull(this.d.getNextDay());
        System.out.println(this.d.toString());
    }
}
