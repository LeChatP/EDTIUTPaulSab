package me.lechatp.edtiutpaulsab;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import me.lechatp.edtiutpaulsab.sync.DailyNotifications;
import me.lechatp.edtiutpaulsab.sync.UpdaterWorker;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {
        private static final int REQUEST_CODE_ALERT_RINGTONE = 928;
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            bindPreference("ringtone_system_settings");
            bindPreference("sync_frequency");

        }

        private void bindPreference(String key){
            Preference p = findPreference(key);
            if (p != null) {
                p.setOnPreferenceChangeListener(this);
            }
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if ("sync_frequency".equals(preference.getKey())) {
                String newvalue = (String) newValue;
                PeriodicWorkRequest saveRequest =
                        new PeriodicWorkRequest.Builder(
                                UpdaterWorker.class,
                                Integer.valueOf(newvalue),
                                TimeUnit.HOURS)
                                .build();
                WorkManager.getInstance(Objects.requireNonNull(this.getActivity()))
                        .enqueueUniquePeriodicWork(getString(R.string.work_name_sync), ExistingPeriodicWorkPolicy.KEEP, saveRequest);
            }else if ("notifications_new_message".equals(preference.getKey())) {
                DailyNotifications.initNotifications(this.getActivity(), (Boolean) newValue);
            }
            return true;
        }

        /**
         * @param preference preference clicked
         * @return if successful
         * TODO: Refactor for API managing
         */
        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            if (preference.getKey().equals("notifications_new_message_ringtone")) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI);

                String existingValue = getRingtonePreferenceValue();
                if (existingValue != null) {
                    if (existingValue.length() == 0) {
                        // Select "Silent"
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                    } else {
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(existingValue));
                    }
                } else {
                    // No ringtone has been selected, set to the default
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
                }

                startActivityForResult(intent, REQUEST_CODE_ALERT_RINGTONE);
                return true;
            } else if(preference.getKey().equals("ringtone_system_settings")){
                startActivity(new Intent("android.settings.APP_NOTIFICATION_SETTINGS").putExtra("android.provider.extra.APP_PACKAGE", Objects.requireNonNull(this.getContext()).getPackageName()));
                DailyNotifications.initNotifications(this.getContext(),true);
                return true;
            } else {
                return super.onPreferenceTreeClick(preference);
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == REQUEST_CODE_ALERT_RINGTONE && data != null) {
                Uri ringtone = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                if (ringtone != null) {
                    setRingtonPreferenceValue(ringtone.toString());
                } else {
                    // "Silent" was selected
                    setRingtonPreferenceValue("");
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }

        private void setRingtonPreferenceValue(String uri) {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(this.getContext()));
                sharedPrefs.edit()
                        .putString("ringtone_uri", uri)
                        .apply();
        }

        private String getRingtonePreferenceValue() {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(this.getContext()));
            Uri configuredUri = Uri.parse(sharedPrefs.getString("ringtone_uri", Settings.System.DEFAULT_RINGTONE_URI.toString()));
            return configuredUri.getPath();
        }
    }
}