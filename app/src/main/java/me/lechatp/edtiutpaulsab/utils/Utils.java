package me.lechatp.edtiutpaulsab.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import me.lechatp.edtiutpaulsab.edt.EDT;

/**
 * Classe Utilitaire
 */
public class Utils {

    private static final String XMLEXP = "<?xml",
            XMLEND = ">",
            CHARSET = "UTF-8",
            TIMEZONE="UTC",
            REPLACE_1 = "\\n",
            REPLACE_2 = "\\\\n",
            REPLACEMENT_2 = " : ",
            EMPTY_STRING = "",
            REPLACE_3 = "\\\\",
            CARRIAGE = "\n";
    private static final String YYYY_MMDD_T_HHMMSS_Z = "yyyyMMdd'T'HHmmss'Z'";
    private static final String DD_MM_YYYY = "dd/MM/yyyy";
    private static final String HH_MM = "HH:mm";
    private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    private static final String EEEE_DD_MMM = "EEEE dd MMM";
    private static final int HOUR = (1000 * 60 * 60);

    public static Document getDocument(String xml) throws ParserConfigurationException, IOException, SAXException, StringIndexOutOfBoundsException {
        String xmlString = xml.substring(xml.indexOf(XMLEXP));
        xmlString = xmlString.substring(0,xmlString.lastIndexOf(XMLEND)+1);
        Document doc;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            doc = builder.parse(new InputSource(new StringReader(xmlString)));
        return doc;
    }
    public static String readURL(URL url) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), CHARSET));
        StringBuilder file = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
        {
            file.append(line.replaceAll(REPLACE_1, EMPTY_STRING).replaceAll(REPLACE_2, REPLACEMENT_2).replaceAll(REPLACE_3, EMPTY_STRING)).append(CARRIAGE);
        }
        reader.close();
        return file.toString();
    }

    public static Calendar getDate(String ics) throws ParseException {
        Calendar cal = Calendar.getInstance(
                TimeZone.getTimeZone(
                        "UTC"+String.valueOf(
                                -(Calendar.getInstance().getTimeZone().getRawOffset()
                                +Calendar.getInstance().getTimeZone().getDSTSavings()
                                 )/HOUR)));
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MMDD_T_HHMMSS_Z,Locale.getDefault());
        sdf.setTimeZone( TimeZone.getTimeZone( TIMEZONE ) );
        cal.setTime(sdf.parse(ics));
        return cal;
    }

    public static Calendar removeTimeOf(Calendar c) {
        Calendar calendar = (Calendar)c.clone();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static boolean sameDay(Calendar a,Calendar b){
        return a.get(Calendar.DAY_OF_MONTH) == b.get(Calendar.DAY_OF_MONTH) && a.get(Calendar.YEAR) == b.get(Calendar.YEAR) && a.get(Calendar.MONTH) == b.get(Calendar.MONTH);
    }

    public static String dayToString(Calendar c){
        SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY,Locale.getDefault());
        //sdf.setTimeZone( TimeZone.getTimeZone( TIMEZONE ) );
        return sdf.format(c.getTime());
    }

    public static String hourToString(Calendar c){
        SimpleDateFormat sdf = new SimpleDateFormat(HH_MM,Locale.getDefault());
        //sdf.setTimeZone( TimeZone.getTimeZone( TIMEZONE ) );
        return sdf.format(c.getTime());
    }

    static Calendar getDatetime(String s){
        Calendar r = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS,Locale.getDefault());
        //sdf.setTimeZone( TimeZone.getTimeZone( TIMEZONE ) );
        try {
            r.setTime(sdf.parse(s));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return r;
    }
    static String toDatetime(Calendar s){
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS,Locale.getDefault());
        return sdf.format(s.getTime());
    }

    private static String readableDate(Calendar c, Locale l){
        SimpleDateFormat sdf = new SimpleDateFormat(EEEE_DD_MMM,l);
        //sdf.setTimeZone( TimeZone.getDefault() );
        return sdf.format(c.getTime());
    }

    public static Spanned getSpannedText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        else //noinspection deprecation
            return Html.fromHtml(text);
    }

    public static URL changeExtension(URL f, String newExtension) throws MalformedURLException {
        int last = f.toString().lastIndexOf('.');
        String url = f.toString().substring(0,last+1);
        return new URL(url + newExtension);
    }

    public static String getStringJour(EDT edt, int tabPosition) throws NoSuchElementException {
        return Utils.readableDate(edt.getJour(tabPosition).getDate(), Locale.getDefault());
    }

    public static Calendar longToCalendar(long l){
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(l);
        return c;
    }

}
