package me.lechatp.edtiutpaulsab;

import me.lechatp.edtiutpaulsab.edt.Creneau;

/**
 * Classe contenant les informations nécéssaire à l'affichage d'un ami
 */
public class Friend {
    private String group;
    private String friend;
    private Creneau c;
    Friend(String friend, String group){
        this.friend = friend;
        this.group = group;
    }

    String getGroup(){
        return group;
    }

    String getFriend() {
        return friend;
    }

    public void setCreneau(Creneau c) {
        this.c = c;
    }

    public Creneau getCreneau() {
        return c;
    }
}
