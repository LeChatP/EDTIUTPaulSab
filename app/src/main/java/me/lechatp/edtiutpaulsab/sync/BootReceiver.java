package me.lechatp.edtiutpaulsab.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import me.lechatp.edtiutpaulsab.R;

public class BootReceiver extends BroadcastReceiver {

    private static final String PREF_DATA_SYNC = "pref_data_sync";
    private static final String DEF_VALUE = "1";
    private static final String SYNC_FREQUENCY_KEY = "sync_frequency";

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (Objects.requireNonNull(intent.getAction())) {
            default:
                SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA_SYNC,Context.MODE_PRIVATE);
                PeriodicWorkRequest saveRequest =
                        new PeriodicWorkRequest.Builder(
                                UpdaterWorker.class,
                                sharedPref.getInt(SYNC_FREQUENCY_KEY, Integer.parseInt(DEF_VALUE)),
                                TimeUnit.HOURS)
                                .build();
                WorkManager.getInstance(context)
                        .enqueueUniquePeriodicWork(context.getString(R.string.work_name_sync), ExistingPeriodicWorkPolicy.KEEP,saveRequest);
                // Notif
                PeriodicWorkRequest saveRequestNotif =
                        new PeriodicWorkRequest.Builder(
                                UpdaterWorker.class,
                                Integer.parseInt(DEF_VALUE),
                                TimeUnit.DAYS)
                                .build();
                WorkManager.getInstance(context)
                        .enqueueUniquePeriodicWork(context.getString(R.string.work_name_sync), ExistingPeriodicWorkPolicy.KEEP,saveRequestNotif);
                break;
        }
        DailyNotifications.initNotifications(context,true);
    }
}
