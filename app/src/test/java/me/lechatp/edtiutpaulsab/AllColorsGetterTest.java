package me.lechatp.edtiutpaulsab;

import android.util.Log;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import me.lechatp.edtiutpaulsab.edt.Departement;
import me.lechatp.edtiutpaulsab.edt.DepartementEnum;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.utils.Utils;

public class AllColorsGetterTest {
    private class AllColors{
        private Map<String, Set<String>> allcolors;

        public AllColors(){
            this.allcolors = new HashMap<>();
        }

        public void put(String color,String type){
            if(!this.allcolors.containsKey(color)) this.allcolors.put(color,new HashSet<String>());
            this.allcolors.get(color).add(type);
        }
        public String toString(){
            return this.allcolors.toString();
        }
    }

    @Before
    public void setUp() {

    }
    @After
    public void tearDown(){
    }
    @Test
    public void getAllColors(){

        AllColors all = new AllColors();
        for(DepartementEnum de : DepartementEnum.values()){
            Departement d = new Departement(de);
            d.fillDepartement(null);
            Assert.assertNotNull(d);
            for(EDT e : d.getEdt()) {
                try {
                    Document doc = Utils.getDocument(Utils.readURL(Utils.changeExtension(e.getLink(), "xml")));

                    XPathFactory factory = XPathFactory.newInstance();
                    XPath xPath = factory.newXPath();
                    NodeList res = null;
                    try {
                        res = (NodeList) xPath.compile("//event[@colour]").evaluate(doc, XPathConstants.NODESET);
                    } catch (XPathExpressionException ex) {
                        ex.printStackTrace();
                    }
                    if (res != null) {
                        for (int i = 1; i < res.getLength(); i++) {
                            Node item = res.item(i);
                            String color = item.getAttributes().getNamedItem("colour").getNodeValue();
                            NodeList nl = item.getChildNodes();
                            for (int j = 1;j<nl.getLength();j++){
                                Node nodetype = nl.item(j);
                                if(nodetype.getNodeName().equalsIgnoreCase("category")){
                                    String type = nodetype.getTextContent();
                                    all.put(color,type);
                                }
                            }
                        }
                    }
                } catch (MalformedURLException e1) {
                } catch (ParserConfigurationException e1) {
                } catch(FileNotFoundException exce) {
                    //LA yes life
                } catch (IOException e1) {
                } catch (SAXException e1) {
                }
            }

        }
        System.out.println(all);
    }

}
