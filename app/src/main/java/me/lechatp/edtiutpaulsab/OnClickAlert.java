package me.lechatp.edtiutpaulsab;

import android.app.AlertDialog;
import android.view.View;

public class OnClickAlert implements View.OnClickListener {

    private final AlertDialog dialog;

    public OnClickAlert(AlertDialog dialog){
        this.dialog = dialog;
    }

    @Override
    public void onClick(View v) {
        this.dialog.show();
    }
}
