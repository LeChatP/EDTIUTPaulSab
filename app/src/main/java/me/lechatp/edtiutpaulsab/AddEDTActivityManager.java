package me.lechatp.edtiutpaulsab;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

import androidx.fragment.app.FragmentManager;
import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.edt.Departement;
import me.lechatp.edtiutpaulsab.edt.DepartementEnum;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.networking.DownloadCallback;
import me.lechatp.edtiutpaulsab.sync.DailyNotifications;
import me.lechatp.edtiutpaulsab.utils.SqLite;

/**
 * UI Manager to add EDT
 */
public class AddEDTActivityManager implements AddEDT {
    private FragmentManager fm;
    private SqLite db;
    private Departement d;
    private final Spinner dpts;
    private final RadioGroup types;
    private final Spinner groupes;
    private String selectedDPT;
    private final ProgressBar pb;
    private boolean departementDownloaded;
    private final Activity a;
    private EditText nom;
    private boolean friends;
    private DownloadCallback<CelcatNetworkType> callback;

    /**
     * Constructor for manager, needs all UI elements
     * @param a current activity
     * @param fm current Fragment manager (to download items)
     * @param callback the handler for downloading CelcatNetworkType
     * @param friend indicates if it's for add friend or not
     */
    AddEDTActivityManager(Activity a, FragmentManager fm, DownloadCallback<CelcatNetworkType> callback, boolean friend) {
        this.callback = callback;
        this.a = a;
        this.dpts = this.a.findViewById(R.id.dept);
        this.groupes = this.a.findViewById(R.id.groupes);
        this.types = this.a.findViewById(R.id.typeEDT);
        Button buttonInsertAmis = this.a.findViewById(R.id.buttonInsertAmis);
        buttonInsertAmis.setOnClickListener(this);
        this.pb = this.a.findViewById(R.id.progressBar);
        this.dpts.setOnItemSelectedListener(this);
        this.types.setOnCheckedChangeListener(this);
        this.db = SqLite.getInstance(this.a);
        this.departementDownloaded = false;
        this.friends = friend;
        this.fm = fm;
        this.nom = this.a.findViewById(R.id.editNom);
        if (!this.friends) {
            this.nom.setVisibility(View.GONE);
        }
        ArrayList<String> spinnerArray = new ArrayList<>();
        for (DepartementEnum de : DepartementEnum.values()) {
            spinnerArray.add(de.getName());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this.a, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        this.dpts.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == this.dpts) {
            this.selectedDPT = parent.getItemAtPosition(position).toString();
            for (DepartementEnum de : DepartementEnum.values()) {
                if (de.getName().equalsIgnoreCase(this.selectedDPT)) {
                    this.d = new Departement(de);
                    this.d.setActivityCallback(this);
                    this.d.fillDepartement(this.fm);
                    this.departementDownloaded = false;
                    this.a.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    this.pb.setVisibility(View.VISIBLE);
                    return;
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        this.selectedDPT = null;
        this.d = null;
        this.groupes.setAdapter(new ArrayAdapter<>(a, android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>()));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        fillEDTS();
    }

    private void fillEDTS(){
        if (this.selectedDPT != null && this.departementDownloaded && this.types.getCheckedRadioButtonId() != -1) {
            ArrayList<String> spinnerArray = new ArrayList<>();
            for (EDT edt : this.d.getEdt()) {
                String s = ((RadioButton) this.types.findViewById(this.types.getCheckedRadioButtonId())).getText().toString();
                if (edt.getType().getNamingType(this.a.getResources()).equalsIgnoreCase(s)) {
                    spinnerArray.add(edt.getName());
                }
            }
            Collections.sort(spinnerArray);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this.a, android.R.layout.simple_spinner_dropdown_item, spinnerArray);

            this.groupes.setAdapter(spinnerArrayAdapter);
            this.groupes.setSelection(0);
        }
    }

    @Override
    public void onClick(View v) {
        if (this.departementDownloaded && this.groupes.getCount() != 0) {
            for (EDT edt : this.d.getEdt()) {
                if (edt.getName().equalsIgnoreCase(this.groupes.getSelectedItem().toString())) {
                    edt.setActivityCallback(this);
                    if (this.friends && db.isAmisExist(nom.getText().toString())) {
                        Toast.makeText(this.a, this.a.getString(R.string.already_used), Toast.LENGTH_LONG).show();
                        return;
                    }
                    try {
                        edt.fillEDT(fm);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
            a.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            Toast.makeText(this.a, this.a.getString(R.string.please), Toast.LENGTH_LONG).show();
        }

        this.pb.setVisibility(View.GONE);
    }

    /**
     * when download is a EDT type
     * @param result EDT downloaded
     */
    private void updateFromDownload(EDT result) {
        if (result != null) {
            if(!this.friends && !this.db.isFirstUse()){
                this.db.removeMineEDT();
                DailyNotifications.initNotifications(this.a,false);
            }
            if(!this.db.insertDataEdt(result.getName(), result.getType().name(), result.getLink().toString(), !this.friends)){
                this.db.setMine(result.getName());
            }
            this.db.insertDataCreneaux(result.getName(), result.getAllCreneaux());
            if (this.friends)
                this.db.insertDataAmis(nom.getText().toString(), result.getName());

        }
    }

    /**
     * when download is a Departement type
     * @param d Departement downloaded
     */
    private void updateFromDownload(Departement d) {
        if (this.types.getCheckedRadioButtonId() != -1) {
            ArrayList<String> spinnerArray = new ArrayList<>();
            for (EDT edt : d.getEdt()) {
                if (edt.getType().getNamingType(this.a.getResources()).equalsIgnoreCase(this.types.findViewById(this.types.getCheckedRadioButtonId()).toString())) {
                    spinnerArray.add(edt.getName());
                }
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this.a, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
            Collections.sort(spinnerArray);
            this.groupes.setAdapter(spinnerArrayAdapter);
        }
        this.departementDownloaded = true;
        fillEDTS();
    }

    /**
     * Handle download content
     * @param result Element downloaded
     */
    @Override
    public void updateFromDownload(CelcatNetworkType result) {
        if (result != null) {
            if (result instanceof EDT) {
                this.updateFromDownload((EDT) result);
            }
            if (result instanceof Departement) {
                this.updateFromDownload((Departement) result);
            }
        }
        this.callback.updateFromDownload(result);
        a.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        this.pb.setVisibility(View.GONE);
        this.callback.finishDownloading();
    }

    public NetworkInfo getActiveNetworkInfo() {
        this.callback.getActiveNetworkInfo();
        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.a.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo();
    }

    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch (progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:
                this.pb.setVisibility(View.GONE);
                a.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                break;
            case Progress.CONNECT_SUCCESS:
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:

                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                break;
        }
        this.callback.onProgressUpdate(progressCode, percentComplete);
    }

    public void finishDownloading() {
    }
}
