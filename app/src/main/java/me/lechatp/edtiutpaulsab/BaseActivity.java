package me.lechatp.edtiutpaulsab;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.networking.DownloadCallback;
import me.lechatp.edtiutpaulsab.sync.UpdaterWorker;
import me.lechatp.edtiutpaulsab.utils.SqLite;

/**
 * Activity used for Navigation drawer and settings
 */
public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DownloadCallback<CelcatNetworkType> {

    public static final String DEF_VALUE = "1";
    public static final String SYNC_FREQUENCY_KEY = "sync_frequency";
    private static final String BUTTON = "BUTTON";
    private SqLite db;
    protected SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutId());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(getIntent().hasExtra(BUTTON))navigationView.setCheckedItem(getIntent().getIntExtra(BUTTON,0));

        this.db=SqLite.getInstance(getApplicationContext());
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
    }


    protected abstract int getLayoutId();

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }else if(id == R.id.change_edt_bar){
            Intent i = new Intent(this,FirstUseActivity.class);
            startActivity(i);
            return true;
        }else if(id == R.id.action_sync){
            OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(UpdaterWorker.class).build();
            WorkManager.getInstance(getApplicationContext()).enqueue(work);
            Toast.makeText(getApplicationContext(),getString(R.string.changing),Toast.LENGTH_LONG).show();
            if(this instanceof MainActivity){
                ((MainActivity) this).reload();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int checkednav = item.getItemId();

        if(checkednav == R.id.mi_friends && !(this instanceof FriendsActivity)){
            Intent i = new Intent(this,FriendsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(BUTTON,R.id.mi_friends);
            startActivity(i);
        }else if(checkednav == R.id.mi_edt){
            if( this instanceof MainActivity ){
                MainActivity ma = (MainActivity) this;
                if(!ma.getEdt().getName().equalsIgnoreCase(SqLite.getInstance(this).getMyEDT().getName())){
                    Intent i = new Intent(this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra(BUTTON,R.id.mi_edt);
                    startActivity(i);
                }
            }else {
                Intent i = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra(BUTTON,R.id.mi_edt);
                startActivity(i);
            }
        }else if(checkednav == R.id.mi_settings){
            Intent i = new Intent(this, SettingsActivity.class);
            i.putExtra(BUTTON,R.id.mi_settings);
            startActivity(i);
        }else if(checkednav == R.id.mi_donation){
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.me/EBilloir"));
            startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public NetworkInfo getActiveNetworkInfo(){
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
    }

    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch(progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:
                Toast.makeText(this,getString(R.string.error_occur),Toast.LENGTH_LONG).show();
                break;
            case Progress.CONNECT_SUCCESS:
            case Progress.GET_INPUT_STREAM_SUCCESS:
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                break;
        }
    }
    protected SqLite getDb(){
        return this.db;
    }
}
