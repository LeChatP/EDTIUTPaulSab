package me.lechatp.edtiutpaulsab;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import me.lechatp.edtiutpaulsab.edt.CreneauColor;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * Classe adaptant l'affichage d'un creneau dans un RecyclerView
 */
public class FriendsArrayAdapter extends ArrayAdapter<Friend> {
    private Context mContext;
    private List<Friend> creneauList;

    FriendsArrayAdapter(@NonNull Context context, @NonNull List<Friend> list) {
        super(context, 0 , list);
        this.mContext = context;
        this.creneauList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.creneau_view,parent,false);
        Friend c = this.creneauList.get(position);
        TextView name = listItem.findViewById(R.id.creneau);
        if(c.getCreneau() != null) {
            name.setText(
                    Utils.getSpannedText(String.format(this.mContext.getString(R.string.friendFormat),
                            c.getFriend(),
                            c.getGroup(),
                            Utils.hourToString(c.getCreneau().getDebut()),
                            Utils.hourToString(c.getCreneau().getFin()),
                            c.getCreneau().getMatiere(),
                            c.getCreneau().getType(),
                            c.getCreneau().getSalle(),
                            c.getCreneau().getEnseignant())));
            name.setBackgroundResource(CreneauColor.textOf(c.getCreneau().getType()).getID());

        }
        else {
            name.setText(Utils.getSpannedText(String.format(this.mContext.getString(R.string.friendFormatMessage),
                    c.getFriend(),
                    this.getContext().getString(R.string.notStudy))));
            name.setBackgroundResource(android.R.color.darker_gray);
        }
        return listItem;
    }
}
