package me.lechatp.edtiutpaulsab;

import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.Toast;

import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.edt.EDT;

/**
 * Activité de première utilisation
 */
public class FirstUseActivity extends AppCompatActivity implements AddEDT {

    private AddEDTActivityManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_use);
        this.manager = new AddEDTActivityManager(this,getSupportFragmentManager(),this,false);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.manager.onItemSelected(parent,view,position,id);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        this.manager.onNothingSelected(parent);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        this.manager.onCheckedChanged(group,checkedId);

    }

    @Override
    public void onClick(View v) {
        this.manager.onClick(v);
    }

    @Override
    public void updateFromDownload(CelcatNetworkType result) {
        if(result instanceof EDT){
            Intent i = new Intent(FirstUseActivity.this,MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public NetworkInfo getActiveNetworkInfo(){
        return null;
    }

    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch(progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:
                Toast.makeText(this,getString(R.string.error_occur),Toast.LENGTH_LONG).show();
                break;
            case Progress.CONNECT_SUCCESS:
            case Progress.GET_INPUT_STREAM_SUCCESS:
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                break;
        }
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    public void finishDownloading() {
    }
}
