package me.lechatp.edtiutpaulsab;

import android.util.Log;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Set;

import me.lechatp.edtiutpaulsab.edt.Creneau;
import me.lechatp.edtiutpaulsab.edt.Departement;
import me.lechatp.edtiutpaulsab.edt.DepartementEnum;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.edt.Jour;


public class DepartementTest {
    private Departement d;
    @Before
    public void setUp() throws MalformedURLException {
        this.d = new Departement(DepartementEnum.INFO.getName(),DepartementEnum.INFO.getUrl());
    }
    @After
    public void tearDown(){
        this.d = null;
    }
    @Test
    public void testDepartement() throws MalformedURLException {
        this.d = new Departement("test","http://www.google.com");
    }
    @Test
    public void testGetName(){
        Assert.assertEquals(this.d.getName(), "info");
    }

    /**
     * This test gives the maximal length of each fields for SQLite table
     */
    @Test
    public void testFillDepartement() {
        this.d.fillDepartement(null);
        Assert.assertFalse(this.d.getEdt().isEmpty());
        int ens = 0;
        int type = 0;
        int matiere = 0;
        int salle = 0;
        int edtname = 0;
        for(EDT e : this.d.getEdt()) {
            edtname = e.getName().length() > edtname ?  e.getName().length():edtname;
            try {
                e.fillEDT(null);
            }catch(Exception exception){

            }
            for (Jour j : e.getJours()){
                for(Creneau c : j.getCreneaux()) {
                    ens = c.getEnseignant().length() > ens ? c.getEnseignant().length() : ens;
                    type = c.getType().length() > type ? c.getType().length() : type;
                    matiere = c.getMatiere().length() > matiere ? c.getMatiere().length() : matiere;
                    salle = c.getSalle().length() > salle ? c.getSalle().length() : salle;
                }

            }
        }
        System.out.println("Taille enseignant : "+ens);
        System.out.println("Taille typeCreneau : "+type);
        System.out.println("Taille matiere : "+matiere);
        System.out.println("Taille salle : "+salle);
        System.out.println("Taille edtname : "+edtname);
    }
    @Test
    public void testTypeCreneauxDepartement() {
        this.d.fillDepartement(null);
        Assert.assertFalse(this.d.getEdt().isEmpty());
        HashSet<String> enseignants = new HashSet<>();
        HashSet<String> types = new HashSet<>();
        HashSet<String> matieres = new HashSet<>();
        HashSet<String> salles = new HashSet<>();
        for(EDT e : this.d.getEdt()) {
            System.out.println("TEST Bouble - New Boucle");
            try {
                e.fillEDT(null);
            }catch(Exception exception){

            }
            for (Jour j : e.getJours()){
                for(Creneau c : j.getCreneaux()) {
                    enseignants.add(c.getEnseignant());
                    types.add(c.getType());
                    matieres.add(c.getMatiere());
                    salles.add(c.getSalle());
                }
            }
        }
        for(String ens : enseignants) System.out.println("TEST - Enseignant "+ens);
        for(String ens : types) System.out.println("TEST - types "+ens);
        for(String ens : matieres) System.out.println("TEST - matieres "+ens);
        for(String ens : salles) System.out.println("TEST - Salles "+ens);
    }
}
