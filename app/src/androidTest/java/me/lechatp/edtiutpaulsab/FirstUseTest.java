package me.lechatp.edtiutpaulsab;

import android.widget.RadioGroup;
import android.widget.Spinner;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.intent.OngoingStubbing;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import me.lechatp.edtiutpaulsab.edt.DepartementEnum;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.Intents.intending;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class FirstUseTest {

    @Rule
    public ActivityTestRule<FirstUseActivity> activityRule =
            new ActivityTestRule<>(FirstUseActivity.class);


    @Test
    public void testDepartements(){
        Intents.init();
        int i = 0;
        for(DepartementEnum de : DepartementEnum.values()){
            onView(withId(R.id.dept)).perform(click());
            onData(allOf(is(instanceOf(String.class)))).atPosition(i).perform(click());
            forRadioGroup();
            i++;
        }

    }
    private void forRadioGroup(){
        forSpinnerItem(R.id.type_group);
        forSpinnerItem(R.id.type_ens);
    }
    private void forSpinnerItem(int id){
        onView(withId(id)).perform(click());
        Spinner s = this.activityRule.getActivity().findViewById(R.id.groupes);
        for(int i = 0 ; i<s.getCount();i++){
            onView(withId(R.id.groupes)).perform(click());
            onData(allOf(is(instanceOf(String.class)))).atPosition(i).perform(click());
            onView(withId(R.id.buttonInsertAmis)).perform(click());
            changeEDT();
            onView(withId(id)).perform(click());
        }
    }
    private void changeEDT(){
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.change_edt)).perform(click());
    }
}
