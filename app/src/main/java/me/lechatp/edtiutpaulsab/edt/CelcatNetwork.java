package me.lechatp.edtiutpaulsab.edt;

import androidx.fragment.app.FragmentManager;

import java.net.MalformedURLException;
import java.net.URL;

import me.lechatp.edtiutpaulsab.networking.DownloadCallback;
import me.lechatp.edtiutpaulsab.networking.NetworkFragment;

/**
 * this class implements a part of Network system for Celcat objects
 * it manage NetworkFragment
 */
public abstract class CelcatNetwork implements CelcatNetworkType {
    private String name;
    private URL link;
    private DownloadCallback<CelcatNetworkType> activityCallback;



    private NetworkFragment nf;

    CelcatNetwork(String name, URL url){
        this.name = name;
        this.link = url;
    }

    CelcatNetwork(String name, String url){
        this.name = name;
        try {
            this.link = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    void initFragment(FragmentManager fm){
        this.nf = NetworkFragment.getInstance(fm);
        this.nf.setURL(this.link.toString());
        this.nf.setCallback(this);
    }

    void startDownload(){
        this.nf.startDownload();
    }

    public String getName() {
        return name;
    }

    public URL getLink() {
        return link;
    }

    DownloadCallback<CelcatNetworkType> getActivityCallback() {
        return activityCallback;
    }

    public void setActivityCallback(DownloadCallback<CelcatNetworkType> activityCallback) {
        this.activityCallback = activityCallback;
    }

    @Override
    public void finishDownloading(){
        if (this.nf != null) {
            this.nf.cancelDownload();
        }
    }
}
