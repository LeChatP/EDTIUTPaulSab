package me.lechatp.edtiutpaulsab.edt;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import me.lechatp.edtiutpaulsab.OnClickAlert;
import me.lechatp.edtiutpaulsab.R;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * Classe pour adapter l'affichage des creneaux dans les RecyclerView
 */
public class CreneauArrayAdapter extends ArrayAdapter<Creneau>{
    private Context mContext;
    private List<Creneau> creneauList;

    public CreneauArrayAdapter(@NonNull Context context, @NonNull List<Creneau> list) {
        super(context, 0 , list);
        this.mContext = context;
        this.creneauList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.creneau_view,parent,false);
        Creneau c = this.creneauList.get(position);
        TextView name = listItem.findViewById(R.id.creneau);
        name.setText(
                Utils.getSpannedText(String.format(this.mContext.getString(R.string.creneauFormat),
                        Utils.hourToString(c.getDebut()),
                        Utils.hourToString(c.getFin()),
                        c.getMatiere(),
                        c.getType(),
                        c.getSalle(),
                        c.getEnseignant())));
        name.setBackgroundResource(CreneauColor.textOf(c.getType()).getID());
        if(c.hasDetails()){
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage(c.getDetails())
                    .setCancelable(true)
                    .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
            AlertDialog alert = builder.create();
            ImageView image = listItem.findViewById(R.id.info);
            image.setVisibility(View.VISIBLE);
            image.setOnClickListener(new OnClickAlert(alert));
        }
        return listItem;
    }
}
