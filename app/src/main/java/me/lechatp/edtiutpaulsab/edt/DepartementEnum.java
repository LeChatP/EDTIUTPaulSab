package me.lechatp.edtiutpaulsab.edt;

/**
 * Liste des départements avec leur URL
 */
public enum DepartementEnum {
    GCGP("gcgp","gcgp/"),
    GCCD("gccd","gcil/"),
    GEAR("gear","gear/"),
    GMP("gmp","gmp/"),
    INFO("info","info/"),
    GE2I("ge2i","ge2i/"),
    GEAP("geap","geap/"),
    IC("ic","ic/"),
    MPH("mph","mph/"),
    TC("tc","tc/"),
    CH1A("ch 1A","ch/chimie1a/"),
    ch2A("ch 2A","ch/chimie2A/"),
    MMI("MMI","mmi/profs/"),
    PEC("PEC","pec/"),
    LPCAQ("LP caq","ch/caq/"),
    LPCOM2WEB("LP com2web","mmi/com2web/"),
    LPCQMPI("LP cqmpi","pec/cqmpi/"),
    LPGF("LP gf","ch/lpgf/"),
    LPDREAM("LP dream","mmi/dream/"),
    LPMC2A("LP mc2a","tcc/mc2a/"),
    LPPALI("LP Pali","pec/pali/");

    private final String name;
    private final String url;
    private static final String ROOT = "https://www.planning.iut-tlse3.fr/";
    private static final String FINDER = "finder.xml";

    DepartementEnum(String name, String url){
        this.name = name;
        this.url = DepartementEnum.ROOT+url+DepartementEnum.FINDER;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
