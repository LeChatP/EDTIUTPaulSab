package me.lechatp.edtiutpaulsab.edt;

import me.lechatp.edtiutpaulsab.networking.DownloadCallback;

/**
 * this interface is to Regroup DownloadCallbackType which can be used for Activities
 * it permits the transformation from DownloadCallback<String> to DownloadCallback<CelcatNetworkType>
 */
public interface CelcatNetworkType extends DownloadCallback<String> {
}
