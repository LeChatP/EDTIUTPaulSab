package me.lechatp.edtiutpaulsab.edt;

import java.util.Comparator;

/**
 * Comparator to order Creneau by Debut
 */
public class DateCreneauComparator implements Comparator<Creneau> {
    @Override
    public int compare(Creneau c1, Creneau c2) { return c1.getDebut().compareTo(c2.getDebut()); }
}