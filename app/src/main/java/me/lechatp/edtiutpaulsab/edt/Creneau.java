package me.lechatp.edtiutpaulsab.edt;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Calendar;

import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * Classe pour les creneaux d'emploi du temps
 * contient toutes les infos
 */
public class Creneau implements Parcelable {

    private long id;
    private Calendar debut, fin;
    private String type, matiere, salle, enseignant, details;

    public Creneau(long id,Calendar debut, Calendar fin,String type,String salle,String matiere,String enseignant ){
        if(debut.getTimeInMillis() > fin.getTimeInMillis())
            throw new IllegalArgumentException("debut cannot be after fin");
        if(!Utils.sameDay(debut,fin))
            throw new IllegalArgumentException("Must be the same day");
        this.debut=debut;
        this.fin = fin;
        this.type = type;
        this.salle = salle;
        this.enseignant = enseignant;
        this.matiere = matiere;
        this.id = id;
    }
    Creneau(Calendar debut, Calendar fin, String type){
        if(debut.getTimeInMillis() > fin.getTimeInMillis())
            throw new IllegalArgumentException("debut cannot be after fin");
        if(!Utils.sameDay(debut,fin))
            throw new IllegalArgumentException("Must be the same day");
        this.debut=debut;
        this.fin = fin;
        this.type = type;
        this.salle = "";
        this.enseignant = "";
        this.matiere = "";
        this.id = -1;
    }

    public Creneau(long id,Calendar debut, Calendar fin,String type,String salle,String matiere,String enseignant, String details ){
        if(debut.getTimeInMillis() > fin.getTimeInMillis())
            throw new IllegalArgumentException("debut cannot be after fin");
        if(!Utils.sameDay(debut,fin))
            throw new IllegalArgumentException("Must be the same day");
        this.debut=debut;
        this.fin = fin;
        this.type = type;
        this.salle = salle;
        this.enseignant = enseignant;
        this.matiere = matiere;
        this.details = details;
        this.id = id;
    }

    public Calendar getDebut() {
        return debut;
    }

    public Calendar getFin() {
        return fin;
    }

    public String getMatiere() {
        return matiere;
    }

    public String getSalle() {
        return salle;
    }

    public String getEnseignant() {
        return enseignant;
    }

    public String getType() {
        return type;
    }

    public String getDetails(){ return details; }

    public long getId(){
        return id;
    }

    void setMatiere(String matiere){ this.matiere = matiere; }

    void setEnseignant(String enseignant) {this.enseignant = enseignant; }

    void setSalle(String salle){
        this.salle = salle;
    }

    void setDetails(String details) { this.details = details; }

    public void setId(long id) {
        if(this.id == -1)this.id = id;
    }

    boolean hasDetails(){return this.details != null && this.details.length() > 0;}

    @NonNull
    @Override
    public String toString(){
        return "Creneau : { "+Utils.hourToString(this.debut)+" -> "
                +Utils.hourToString(this.fin)+" : type='"
                +this.type+"', matiere='"
                +this.matiere+"', salle='"
                +this.salle+"', ens='"
                +this.enseignant+"' details='"
                +this.details+"'}";
    }



    private Creneau(Parcel p){
        this.id=p.readLong();
        this.debut=Utils.longToCalendar(p.readLong());
        this.fin =Utils.longToCalendar(p.readLong());
        this.type = p.readString();
        this.salle = p.readString();
        this.enseignant = p.readString();
        this.matiere = p.readString();
        this.details = p.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.debut.getTimeInMillis());
        dest.writeLong(this.fin.getTimeInMillis());
        dest.writeString(this.type);
        dest.writeString(this.salle);
        dest.writeString(this.enseignant);
        dest.writeString(this.matiere);
        dest.writeString(this.details);
    }

    public static Parcelable.Creator<Creneau> CREATOR = new Creator<Creneau>() {
        @Override
        public Creneau createFromParcel(Parcel source) {
            return new Creneau(source);
        }

        @Override
        public Creneau[] newArray(int size) {
            return new Creneau[size];
        }
    };
}
