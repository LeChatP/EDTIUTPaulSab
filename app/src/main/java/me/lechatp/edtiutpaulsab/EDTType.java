package me.lechatp.edtiutpaulsab;

import android.content.res.Resources;

/**
 * Les types d'emploi du temps disponibles
 */
public enum EDTType{
    GROUP(R.string.group),
    STAFF(R.string.staff);
    int id;
    EDTType(int id){
        this.id = id;
    }
    public String getNamingType(Resources r){
        return r.getString(this.id);
    }
}