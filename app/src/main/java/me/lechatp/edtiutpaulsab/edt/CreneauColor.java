package me.lechatp.edtiutpaulsab.edt;

import me.lechatp.edtiutpaulsab.R;

/**
 * Associe une couleur a un type de créneau
 */
public enum CreneauColor {
    TD("TD",R.color.TD),
    TP("TP",R.color.TP),
    CM("CM",R.color.CM),
    EXAM("Examen",R.color.exam),
    PTUT("projet tutoré",R.color.ptut),
    SOUTIEN("soutien",R.color.soutien),
    PARTIELS("Partiels",R.color.partiels),
    PRERESERV("pré-réservation",R.color.TD),
    RESERV("Réservation",R.color.reserv),
    SOUTENANCE("Soutenance",R.color.soutenance),
    OTHERS("Autres",R.color.others);
    int c;
    String name;
    CreneauColor(String name,int c){
        this.c = c;
        this.name = name;
    }
    public int getID(){return this.c;}
    public static CreneauColor textOf(String c){
        for(CreneauColor cc:CreneauColor.values()){
            if(cc.name.contains(c)) return cc;
        }
        return CreneauColor.OTHERS;
    }
}
