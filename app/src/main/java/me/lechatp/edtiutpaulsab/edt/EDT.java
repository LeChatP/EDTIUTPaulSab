package me.lechatp.edtiutpaulsab.edt;

import android.net.NetworkInfo;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import me.lechatp.edtiutpaulsab.EDTType;
import me.lechatp.edtiutpaulsab.networking.DownloadCallback;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * Les fichiers xml d'emploi du temps peuvent être triés à l'aide des balises <alleventweeks> ou <rawweeks>
 * Note: La pire manière de référencer les semaines
 * C'est pour cela que nous avons priorisé l'utilisation du fichier ICS
 */
public class EDT extends CelcatNetwork implements CelcatNetworkType {
    private static final String VEVENT_1_2 = "(BEGIN:VEVENT\n|END:VEVENT\n){1,2}";
    private static final String DESCRIPTION = "DTSTAMP:([TZ0-9]*)\nDTSTART:([TZ0-9]*)\nDTEND:([TZ0-9]*)\n.*\nSUMMARY:(.*) - .*\nLOCATION:(.*)\nDESCRIPTION:(.*)";
    private static final String MATIERE = "Matière : ([^:]+)";
    private static final String PERSONNEL = "Personnel : ([^:]+)";
    private static final String REMARQUES_CATEGORIES = "Remarques : (.+)CATEGORIES";
    private static final String CARRIAGE = "\n";
    private static final String STRING = "EDT{ name=%s, type=%s, url=%s, jours={\n";
    private EDTType type;
    private SortedMap<Calendar,Jour> jours;

    public EDT(EDTType type, String name, URL edtURL) {
        super(name,edtURL);
        this.type = type;
        this.jours = new TreeMap<>();
    }

    /**
     * Je télécharge l'ICS et lui applique une expression régulière parce que l'xml semble plus complexe à parser et moins optimisé
     */
    public void fillEDT(FragmentManager fm) throws IOException, ParseException {
        if(fm != null) {
            super.initFragment(fm);
            super.startDownload();
        }else{
            fillEDT();
        }
    }
    private void fillEDT() throws ParseException, IOException {
        String ics = Utils.readURL(super.getLink());
        this.setEDT(ics);
    }
    private void setEDT(String ics) throws ParseException {
        String[] events = ics.split(VEVENT_1_2);
        for(int i = 1 ; i< events.length-1;i++){
            String event = events[i];
            Pattern p = Pattern.compile(DESCRIPTION,Pattern.DOTALL);
            Matcher datas = p.matcher(event);
            if(!datas.find())throw new IllegalStateException("ICS is wrong");
            Calendar begin = Utils.getDate(datas.group(2));
            Calendar end = Utils.getDate(datas.group(3));
            String type = datas.group(4);
            String location = datas.group(5);
            Creneau c = new Creneau(begin, end, type);
            c.setSalle(location);
            String desc = datas.group(6);
            p = Pattern.compile(MATIERE);
            Matcher m = p.matcher(desc);
            if(m.find()) c.setMatiere(m.group(1).replaceAll(CARRIAGE,""));
            p = Pattern.compile(PERSONNEL);
            m = p.matcher(desc);
            if(m.find())c.setEnseignant(m.group(1).replaceAll(CARRIAGE,""));
            p = Pattern.compile(REMARQUES_CATEGORIES,Pattern.DOTALL);
            m = p.matcher(desc);
            if(m.find()) {
                String str = m.group(1).replaceAll(CARRIAGE,"");
                c.setDetails(str.substring(0, str.length() - 3));
            }
            this.addCreneau(c);

        }
        //this.jours = this.jours.tailMap(Utils.removeTimeOf(new GregorianCalendar()));
    }

    public void addCreneau(Creneau c){
        for(Jour j : jours.values()){
            if(j.setCreneau(c)) return; //append to existant Jour
        }
        //else creating new Jour
        Jour jour = new Jour(Utils.removeTimeOf(c.getDebut()));
        jour.setCreneau(c);
        this.jours.put(jour.getDate(),jour);
    }
    public SortedSet<Creneau> getAllCreneaux(){
        SortedSet<Creneau> arr = new TreeSet<>(new DateCreneauComparator());
        for(Jour j : this.jours.values()){
            arr.addAll(j.getCreneaux());
        }
        return arr;
    }

    public Jour getToday(){
        Calendar date = new GregorianCalendar();
        return this.jours.get(Utils.removeTimeOf(date));
    }

    /**
     *
     * @return next Jour by today
     */
    public Jour getNextDay() throws NoSuchElementException {
        Calendar date = new GregorianCalendar();
        return this.jours.get(this.jours.tailMap(Utils.removeTimeOf(date)).firstKey());
    }

    public EDTType getType() {
        return type;
    }

    @NonNull
    public String toString(){
        StringBuilder res = new StringBuilder(String.format(STRING, this.getName(), this.getType(), this.getLink().toString()));
        for(Jour j : this.jours.values()){
            res.append(j.toString());
        }
        res = new StringBuilder(res.substring(0, res.length() - 1));
        return res+"}";
    }

    @Override
    public void updateFromDownload(String ics) {
        if(ics !=null) {
            try {
                this.setEDT(ics);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(this.getActivityCallback() !=null)
            this.getActivityCallback().updateFromDownload(this);
        }else{
            if(this.getActivityCallback() !=null)
            this.getActivityCallback().updateFromDownload(null);
        }
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        if(this.getActivityCallback() !=null)
        return this.getActivityCallback().getActiveNetworkInfo();
        return null;
    }

    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        if(this.getActivityCallback() !=null)
        this.getActivityCallback().onProgressUpdate(progressCode,percentComplete);
    }

    @Override
    public void finishDownloading() {
        super.finishDownloading();
        if(this.getActivityCallback() !=null)
        this.getActivityCallback().finishDownloading();
    }

    public void setActivityCallback(DownloadCallback<CelcatNetworkType> activityCallback) {
        super.setActivityCallback(activityCallback);
    }

    public Jour getJour(int position){
        ArrayList<Jour> jours = this.getJours();
        Collections.sort(jours, (o1, o2) -> o1.getDate().compareTo(o2.getDate()));
        return jours.get(position);
    }

    /**
     * @return get day within today
     */
    public ArrayList<Jour> getJours(){
        Calendar c = new GregorianCalendar();
        c = Utils.removeTimeOf(c);
        int i = 0;
        for(Calendar jour : this.jours.keySet()){
            if(jour.compareTo(c) >= 0)
                if(i >0)return new ArrayList<>(this.jours.tailMap(jour).values());
                else return new ArrayList<>(this.jours.values());
            i++;
        }
        return new ArrayList<>(this.jours.values());
    }
}
