package me.lechatp.edtiutpaulsab.sync;

import android.content.Context;

import java.net.UnknownHostException;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.utils.SqLite;

public class UpdaterWorker extends Worker {

    public UpdaterWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        return update(getApplicationContext()) ? Result.success() : Result.failure();
    }

    private boolean update(Context context){
        SqLite db = SqLite.getInstance(context);
        Set<EDT> edts = db.getAllEDT();
        db.beginTransaction();
        db.emptyCreneaux();
        for(EDT edt : edts){
            try {
                edt.fillEDT(null);
            }catch (Throwable t){
                db.rollback();
                return false;
            }
            if(!db.insertDataCreneaux(edt.getName(), edt.getAllCreneaux())){
                db.rollback();
                return false;
            }
        }
        db.updateSync();
        db.commit();
        return true;
    }
}
