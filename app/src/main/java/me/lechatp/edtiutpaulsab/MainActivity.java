package me.lechatp.edtiutpaulsab;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import me.lechatp.edtiutpaulsab.edt.CelcatNetworkType;
import me.lechatp.edtiutpaulsab.edt.Creneau;
import me.lechatp.edtiutpaulsab.edt.CreneauArrayAdapter;
import me.lechatp.edtiutpaulsab.edt.EDT;
import me.lechatp.edtiutpaulsab.edt.Jour;
import me.lechatp.edtiutpaulsab.sync.DailyNotifications;
import me.lechatp.edtiutpaulsab.sync.UpdaterWorker;
import me.lechatp.edtiutpaulsab.utils.Utils;

/**
 * Activité principale affichant l'emploi du temps sur 3 jours
 */
public class MainActivity extends BaseActivity implements Button.OnClickListener{

    private static final String EDT = "EDT";
    private static final String NAME = "NAME";
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    protected ViewPager mViewPager;

    private EDT edt;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(this.getDb().isFirstUse()){
            Intent i = new Intent(MainActivity.this,FirstUseActivity.class);
            startActivity(i);
            finish();
            return;
        }
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setSmoothScrollingEnabled(true);

        Button change;
        if(getIntent().hasExtra(EDT)){
            String name = getIntent().getStringExtra(NAME);
            this.edt = this.getDb().getEDT(getIntent().getStringExtra(EDT));
            change = findViewById(R.id.change_edt);
            ViewGroup vg = (ViewGroup) change.getParent();
            vg.removeView(change);
            Objects.requireNonNull(getSupportActionBar()).setTitle(String.format(getString(R.string.planning_name), name));
        }
        else {
            this.edt = this.getDb().getMyEDT();
            change = findViewById(R.id.change_edt);
            change.setOnClickListener(this);
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        this.mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),this.edt);


        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        Constraints c = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
        PeriodicWorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(
                        UpdaterWorker.class,
                        Integer.parseInt(
                                Objects.requireNonNull(this.prefs.getString(SYNC_FREQUENCY_KEY, DEF_VALUE))),
                        TimeUnit.HOURS)
                        .setConstraints(c)
                        .build();
        WorkManager.getInstance(getApplicationContext())
                .enqueueUniquePeriodicWork(getString(R.string.work_name_sync), ExistingPeriodicWorkPolicy.KEEP,saveRequest);
        DailyNotifications.initNotifications(getApplicationContext(),true);
    }

    public void reload(){
        this.edt = this.getDb().getEDT(this.edt.getName());
        TabLayout tabLayout = findViewById(R.id.tabs);
        int scroll = tabLayout.getScrollX();
        mSectionsPagerAdapter.notifyDataSetChanged();
        tabLayout.setScrollX(scroll);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void updateFromDownload(CelcatNetworkType result) {

    }

    @Override
    public void finishDownloading() {

    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this,FirstUseActivity.class);
        startActivity(i);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String JOUR = "JOUR";

        public PlaceholderFragment() {
            super();
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_edt, container, false);
            if (getArguments() != null) {
                if (getArguments().containsKey(JOUR)) {
                    Jour jour = getArguments().getParcelable(JOUR);
                    ArrayList<Creneau> list;
                    if (jour != null) {
                        list = new ArrayList<>(jour.getCreneaux());
                        ArrayAdapter<Creneau> textArrayAdapter = new CreneauArrayAdapter(this.requireContext(), list);
                        ((ListView) rootView.findViewById(R.id.creneauList)).setAdapter(textArrayAdapter);
                        textArrayAdapter.notifyDataSetChanged();
                    }
                } else {
                    ArrayList<String> nl = new ArrayList<>();
                    nl.add(getString(R.string.nowork));
                    ArrayAdapter<String> noowork = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, nl);
                    this.requireActivity().findViewById(R.id.change_edt).setVisibility(View.VISIBLE);
                    ((ListView) rootView.findViewById(R.id.creneauList)).setAdapter(noowork);
                    noowork.notifyDataSetChanged();
                }
            }
            return rootView;
        }
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private EDT edt;

        SectionsPagerAdapter(FragmentManager fm,EDT edt) {
            super(fm,FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            this.edt = edt;
        }

        @Override
        @NonNull
        public Fragment getItem(int position) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putParcelable(PlaceholderFragment.JOUR,edt.getJour(position));
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return this.edt.getJours().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Utils.getStringJour(this.edt,position);
        }
    }
    public EDT getEdt(){
        return this.edt;
    }
}
